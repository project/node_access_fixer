Node Access Fixer

This module does nothing on its own, other than to attempt to make harmony out
of multiple node-related "access callback" changes made in the hook_menu_alter
implementations of various modules.  It does this by ensuring that its own
"access callback" handler is called for each of several node-related menu paths
('node/%node', 'node/%node/edit', etc.). This handler then calls all other
"access callback" handlers for the given path, returning TRUE only if all
handlers grant access.

RATIONALE:
Drupal's menu system provides for the stipulation of exactly one function to
determine whether or not to grant a user access to a given path. Module
developers who wish to override the default access function may replace it with
one of their own, by implementing hook_menu_alter() and changing the "access
callback" for the given path to a function of their own design.

For the simplest cases, this can work well. However, when more than one module
makes changes to the access callback, things get more complex. When multiple
modules seek to expand or restrict access to the same menu path, developers must
take special care to ensure that all access checks are handled properly.

Developers creating a new module can utilize the Chain Menu Access API module to
ensure that their access callbacks are handled in harmony with other modules'
access callbacks on the same path. However, many popular Drupal contrib modules
don't make use of this method and simply overwrite the access callback, assuming
that no other modules are doing the same thing. On sites running more than one
of these modules, only one is going to have the final say, leading to
unpredictable results.

Node Access Fixer solves this problem for node-related paths, by detecting
changes to access callback on those paths, and ensuring that all access
callbacks are called in order.

FUNCTIONALITY:
Node Access Fixer handles access callback changes for the paths 'node/%node',
'node/%node/edit', and 'node/%node/delete'.

By default, access callbacks on these paths are combined using an OR operator;
that is, access is granted if any of the callbacks returns TRUE, which means
each callback serves to loosen access.  Developers who wish their callback to
restrict access can set the property 'NODE_ACCESS_FIXER_MODE' =
NODE_ACCESS_FIXER_MODE_AND in their hook_menu_alter() implementation.
This will cause the callback to be combined with other callbacks using an AND
operator, meaning the callback will be used to restrict access to the path.
(See examples.)

EXAMPLES:
// Define a new "access callback" value, to be combined using an AND operator.
function my_menu_alter(&$items) {
  $items['node/%node']['access callback'] = '_my_node_access';
  $items['node/%node']['NODE_ACCESS_FIXER_MODE'] = NODE_ACCESS_FIXER_MODE_AND;
}
